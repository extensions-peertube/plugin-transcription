import { createReadStream } from 'fs-extra';
import { MVideoCaptionVideo, MVideoFullLight } from '@peertube/peertube-types/server/types/models';
import { PeerTubeHelpers } from '@peertube/peertube-types/server/types/plugins/register-server-option.model';
import { VideoUpdate } from '@peertube/peertube-types';
import { LoggerInterface } from '../Model';
import { AuthenticatedPeerTubeResponse, PeerTubeApi } from '../PeerTube';
import { TranscriptionWorkerPool } from '../Transcription/TranscriptionWorkerPool';
import { LanguageModelManager } from '../Recognizer';
import { TranscriptionRequest } from '../Transcription/TranscriptionRequest';
import { MVideo } from '@peertube/peertube-types/server/types/models/video/video';

export class VideoController {
  private readonly logger: LoggerInterface;
  private languageModelManager: LanguageModelManager;
  private peerTubeApi: PeerTubeApi;
  private workerPool: TranscriptionWorkerPool;
  private peerTubeVideosHelpers: PeerTubeHelpers['videos'];

  constructor(
    logger: LoggerInterface,
    languageModelManager: LanguageModelManager,
    peerTubeApi: PeerTubeApi,
    peerTubeVideosHelpers: PeerTubeHelpers['videos'],
    workerPool: TranscriptionWorkerPool
  ) {
    this.logger = logger;
    this.languageModelManager = languageModelManager;
    this.peerTubeApi = peerTubeApi;
    this.peerTubeVideosHelpers = peerTubeVideosHelpers;
    this.workerPool = workerPool;
  }

  /**
   * This event is fired when the video is `uploaded`, there isn't a `created` event.
   * The video creation form actually trigger an `updated` event (see below).
   *
   * But this might be useful to trigger transcription sooner if we choose to force transcription
   * without a user-defined language.
   */
  async uploaded() {}

  async updated({
    video,
    res,
  }: {
    video: MVideoFullLight;
    body: VideoUpdate;
    req: Express.Request;
    res: AuthenticatedPeerTubeResponse;
  }) {
    const { language, id } = video;
    this.logger.debug('Updating video', { id, language });

    this.peerTubeApi.setOAuthTokenFromResponse(res);
    const alreadyHasCaption = await this.peerTubeApi.hasVideoCaptionForLanguage(id, language);
    if (alreadyHasCaption) {
      this.logger.info(`Video "${id}" already have a caption for "${language}", skipping.`);
      return;
    }

    if (!language) {
      this.logger.info(`Video "${id}" doesn't have a language defined, skipping.`);
      return;
    }

    const videoFiles = await this.getVideoFiles(id);
    if (videoFiles.length === 0) {
      this.logger.info(`No valid video file found for video "${id}", skipping.`);
      return;
    }

    const modelName = await this.languageModelManager.findOneOrConfigureOneFor(language);
    if (!modelName) {
      this.logger.warn(
        `Couldn't find and setup a language model for video "${id}" in "${language}", skipping transcription.`
      );
      return;
    }

    this.workerPool.runTask({
      task: new TranscriptionRequest(video, videoFiles[0]),
      callback: this.createTaskRunnerCallback(video, res),
    });
  }

  async captionDeleted({
    caption,
    res,
  }: {
    caption: MVideoCaptionVideo;
    req: Express.Request;
    res: AuthenticatedPeerTubeResponse;
  }) {
    const video = await this.peerTubeVideosHelpers.loadByIdOrUUID(caption.videoId);

    if (!video.language) {
      this.logger.info(`Video "${video.id}" doesn't have a language set. Skipping transcription...`);
      return;
    }

    if (video.language !== caption.language) {
      this.logger.info(
        `Deleted caption was in ${caption.language} whereas video is in ${video.language}. Skipping transcription...`
      );
      return;
    }

    const videoFiles = await this.getVideoFiles(video.id);
    if (videoFiles.length === 0) {
      this.logger.info(`No valid video file found for video "${video.id}". Skipping transcription...`);
      return;
    }

    const modelName = await this.languageModelManager.findOneOrConfigureOneFor(video.language);
    if (!modelName) {
      this.logger.warn(
        `Couldn't find and setup a language model for video "${video.id}" in "${video.language}". Skipping transcription...`
      );
      return;
    }

    this.workerPool.runTask({
      task: new TranscriptionRequest(video, videoFiles[0]),
      callback: this.createTaskRunnerCallback(video, res),
    });
  }

  private createTaskRunnerCallback(video: Pick<MVideo, 'id' | 'language'>, res: AuthenticatedPeerTubeResponse) {
    return (e?: Error | null, outputFilePath?: string | null) => {
      if (e) {
        this.logger.log({ level: 'error', message: e });
      }

      if (!e && outputFilePath) {
        this.peerTubeApi.setOAuthTokenFromResponse(res);
        this.peerTubeApi.addVideoCaption(video.id, video.language, createReadStream(outputFilePath)).catch((e) => {
          this.logger.error(
            `Failed to upload caption ${outputFilePath} to video "${video.id}", destroying the stream...`
          );

          throw e;
        });
      }
    };
  }

  private async getVideoFiles(id: number) {
    return await this.peerTubeVideosHelpers
      .getFiles(id)
      .then(({ hls: { videoFiles } }) => videoFiles.sort(({ size: sizeA }, { size: sizeB }) => sizeA - sizeB));
  }
}
