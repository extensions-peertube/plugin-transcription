import { PeerTubeAboutConfigInterface } from '../Model/PeerTubeAboutConfigInterface';

export interface PeerTubeApiInterface {
  getConfigAbout: () => Promise<PeerTubeAboutConfigInterface>;
  getInstanceLanguage: () => Promise<string | undefined>;
  addVideoCaption: (id: number, language: string, captionFile: any) => Promise<unknown>;
}
