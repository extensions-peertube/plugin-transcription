import { SettingsRegistryInterface } from '../SettingsRegistryInterface';

export const settingsRegistry: SettingsRegistryInterface = {
  get: jest.fn(),
  getPrevious: jest.fn(),
  set: jest.fn(),
  register: jest.fn(),
};
