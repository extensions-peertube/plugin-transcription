import { Format, NodeCue, stringifySync } from 'subtitle';
import { outputFile } from 'fs-extra';

export const formatExtensions: Record<Format, string> = {
  WebVTT: 'vtt',
  SRT: 'srt',
};

export function getExtensionFromFormat(format: Format) {
  return formatExtensions[format];
}

export function getFormatFromExtension(extension: string | undefined): Format | undefined {
  if (!extension) {
    return undefined;
  }

  const validFormats: Format[] = Object.keys(formatExtensions).filter<Format>(
    // @ts-ignore
    (format) => String(formatExtensions[format]) === extension
  );
  return validFormats[0];
}

export function getExtension(filePath: string) {
  return filePath.split('.').pop();
}

export function guessFormatFromFileExtension(filePath: string) {
  const extension = getExtension(filePath);
  const format = getFormatFromExtension(extension);

  if (!format) {
    throw new Error(`Couldn't guess a valid subtitle format from ${filePath} file extension.`);
  }

  return format;
}

export async function outputSubtitleFileFromNodeCues(captionFilePath: string, nodeCues: NodeCue[]) {
  const format: Format = guessFormatFromFileExtension(captionFilePath);
  const captionContent = stringifySync(nodeCues, { format });
  return outputFile(captionFilePath, captionContent);
}
