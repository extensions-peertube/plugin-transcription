import { toHumanReadable } from './duration';

describe('duration', () => {
  test('toHumanReadable', async () => {
    const ONE_MINUTE = 60000;
    let humanDuration = toHumanReadable(ONE_MINUTE);
    expect(humanDuration).toEqual('1m');

    humanDuration = toHumanReadable(ONE_MINUTE * 60 + ONE_MINUTE);
    expect(humanDuration).toEqual('1h 1m');
  });
});
