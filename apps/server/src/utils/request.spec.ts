import { get } from './request';

describe('request', () => {
  test('get', async () => {
    await get(`http://localhost:${process.env.PORT || 9001}/`);
  });
});
