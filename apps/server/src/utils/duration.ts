export interface DurationDescriptor {
  duration: number;
  unit: string;
}

export function toHumanReadable(ms: number) {
  const date = new Date(ms);

  const durationDescriptors: DurationDescriptor[] = [
    { duration: date.getUTCHours(), unit: 'h' },
    { duration: date.getUTCMinutes(), unit: 'm' },
    { duration: date.getUTCSeconds(), unit: 's' },
    { duration: date.getUTCMilliseconds(), unit: 'ms' },
  ];

  return durationDescriptors
    .map(toWords)
    .filter((words) => words)
    .join(' ');
}

export function toWords({ duration, unit }: DurationDescriptor) {
  return duration > 0 ? `${duration}${unit}` : '';
}
