import unzip from 'unzipper';
import { move, pathExists, remove } from 'fs-extra';
import { resolve } from 'path';
import { pipeline } from 'stream/promises';
import { LoggerInterface, VoskModelDescriptor } from '../Model';
import { extractLanguage, getStreamPromise } from '../utils';
import { LanguageModelRepository } from '../Repository/LanguageModelRepository';
import { SettingEntry, SettingsRegistryInterface } from '../PeerTube';

export const LANGUAGE_STORAGE_KEY_PREFIX = 'language_';

export class LanguageModelManager {
  private settingsRegistry: SettingsRegistryInterface;
  private languageModelRepository: LanguageModelRepository;
  private logger: LoggerInterface;
  private readonly modelPath: string;
  private availableLanguages: Record<string, string>;

  constructor(
    settingsRegistry: SettingsRegistryInterface,
    languageModelRepository: LanguageModelRepository,
    logger: LoggerInterface,
    modelPath: string,
    availableLanguages: Record<string, string>
  ) {
    this.settingsRegistry = settingsRegistry;
    this.languageModelRepository = languageModelRepository;
    this.logger = logger;
    this.modelPath = modelPath;
    this.availableLanguages = availableLanguages;
  }

  async downloadAndExtractVoskModel({ url, name, lang: locale }: Pick<VoskModelDescriptor, 'url' | 'name' | 'lang'>) {
    await this.downloadAndExtract({ url, name, language: extractLanguage(locale) });
  }

  /**
   * The model is supposed to be stored at `data/models/en`, but we also attempt to delete:
   * - `data/models/vosk-model-en-0.4`
   * - `data/models/vosk-model-en-0.4.zip`
   * In case something went wrong during the installation of the model.
   */
  async deleteModel({ name, language }: Pick<VoskModelDescriptor, 'name'> & { language: string }) {
    try {
      const possibleModelPathEntries = [
        resolve(this.modelPath, name),
        resolve(this.modelPath, `${name}.zip`),
        resolve(this.modelPath, language),
      ];
      await Promise.all(
        possibleModelPathEntries.map(async (pathEntry) => {
          this.logger.debug(`Deleting ${pathEntry} if exists.`);
          await remove(pathEntry);
        })
      );
    } catch (e) {
      this.logger.error(e);
    }
  }

  async downloadAndExtract({ url, name, language }: Pick<VoskModelDescriptor, 'url' | 'name'> & { language: string }) {
    this.logger.debug(`Downloading model ${name}...`);
    await pipeline(await getStreamPromise(url), unzip.Extract({ path: this.modelPath })).catch((e) =>
      this.logger.error(e)
    );
    const sourceDirectory = resolve(this.modelPath, name);
    const exists = await pathExists(sourceDirectory);
    if (!exists) {
      throw new Error(`${sourceDirectory} doesn't exist.`);
    }
    const destinationDirectory = resolve(this.modelPath, language);
    await move(sourceDirectory, destinationDirectory, {
      overwrite: true,
    });
    this.logger.debug(`Model ${name} configured at ${destinationDirectory}`);
  }

  static getSettingKey(language: string) {
    return `${LANGUAGE_STORAGE_KEY_PREFIX}${language}`;
  }

  static isLanguageSettingKey(settingKey: string) {
    return settingKey.includes(LANGUAGE_STORAGE_KEY_PREFIX);
  }

  static getLanguageFromSettingKey(settingKey: string) {
    return settingKey.replace(LANGUAGE_STORAGE_KEY_PREFIX, '');
  }

  async configureFromSettingsEntries(settingsEntries: SettingEntry) {
    await Promise.all(
      Object.keys(settingsEntries)
        .filter(LanguageModelManager.isLanguageSettingKey)
        .map(async (settingKey) => {
          await this.configureFromSettingEntry(settingKey, settingsEntries[settingKey]);
        })
    );
  }

  async configureFromSettingEntry(settingKey: string, value: string | boolean) {
    const model = this.languageModelRepository.getModelByName(value);
    if (!model) {
      this.logger.error(`Couldn't find model named ${value}.`);
      await this.settingsRegistry.set(settingKey, '');
      return;
    }

    const previousModelName = await this.settingsRegistry.getPrevious(settingKey);
    if (previousModelName !== model.name) {
      this.logger.debug(`Changing model ${previousModelName} for ${model.name}.`);
      if (previousModelName) {
        await this.deleteModel({
          name: previousModelName,
          language: LanguageModelManager.getLanguageFromSettingKey(settingKey),
        });
      }
      await this.configureModel(model);
    } else {
      this.logger.debug(
        `Configured model for "${LanguageModelManager.getLanguageFromSettingKey(
          settingKey
        )}" is the same as the previous one: ${previousModelName}.`
      );
    }
  }

  getSettingValueForLanguage(language: string) {
    return this.settingsRegistry.get(LanguageModelManager.getSettingKey(language));
  }

  async findOneOrConfigureOneFor(language: string) {
    const modelName = await this.getSettingValueForLanguage(language);

    if (!modelName) {
      this.logger.debug(`No model configured for "${language}" attempting to find one compatible...`);
      const modelForLanguage = this.languageModelRepository.findSmallestModelForLanguage(language);
      if (modelForLanguage) {
        this.logger.debug(`Found ${modelForLanguage.name} model for ${language}.`);
        await this.configureModel(modelForLanguage);

        return modelForLanguage.name;
      } else {
        this.logger.debug(`Couldn't find a model for ${language}.`);
      }
      return;
    }

    return modelName;
  }

  async configureModel({ url, name, lang: locale }: Pick<VoskModelDescriptor, 'url' | 'lang' | 'name'>) {
    const language = extractLanguage(locale);

    await this.downloadAndExtract({ url, name, language });
    await this.setSettingValueForLanguage(language, name);
  }

  private getLanguageModelsOptionsForLanguage(language: string) {
    const availableModelsForLanguage = this.languageModelRepository.getModelsByLanguage(language);
    return availableModelsForLanguage.map(({ name, size_text }) => ({
      label: `${name} (${size_text})`,
      value: name,
    }));
  }

  public setSettingValueForLanguage(language: string, value: string) {
    this.settingsRegistry.set(LanguageModelManager.getSettingKey(language), value);
  }

  public registerOneForLanguage(language: string) {
    return this.settingsRegistry.register({
      name: LanguageModelManager.getSettingKey(language),
      label: this.availableLanguages[language],
      type: 'select',
      options: this.getLanguageModelsOptionsForLanguage(language),
      private: false,
    });
  }

  public async registerAll() {
    await Promise.all(
      Object.keys(this.availableLanguages).map(async (language) => {
        await this.registerOneForLanguage(language);
      })
    );
  }
}
