import { register } from './index';
import { pluginSettingsManager, registerPluginSetting } from './__mocks__/PeerTube/pluginSettings';
import { pluginStorageManager } from './__mocks__/PeerTube/PluginStorageManager';
import { pluginVideoLanguageManager } from './__mocks__/PeerTube/PluginVideoLanguageManager';

describe('Plugin', () => {
  test('register', async () => {
    await register({
      registerHook: jest.fn(),
      registerSetting: registerPluginSetting,
      getRouter: jest.fn(),
      peertubeHelpers: {
        // @ts-ignore
        logger: console,
        // @ts-ignore
        config: {
          getWebserverUrl: jest.fn(() => `http://localhost:${process.env.PORT || 9001}`),
        },
        // @ts-ignore
        plugin: {
          getDataDirectoryPath: jest.fn(() => 'data/'),
        },
      },
      storageManager: pluginStorageManager,
      settingsManager: pluginSettingsManager,
      videoLanguageManager: pluginVideoLanguageManager,
    });
  });
});
