export interface TranscriptionRequestInterface {
  uuid: string;
  filePath: string;
  language: string;
}
