import { RegisterServerSettingOptions } from '@peertube/peertube-types';
import { RegisterServerOptions } from '@peertube/peertube-types/server/types';
import { Setting } from '../../PeerTube';
import { PluginSettingsManagerInterface } from '../../Model/PluginSettingsManagerInterface';

const storage: Record<string, Setting> = {};

export const pluginSettingsManager: PluginSettingsManagerInterface = {
  getSettings: jest.fn((settingKeys: string[]) =>
    Promise.resolve(
      settingKeys.reduce((settings: Record<string, any>, key) => {
        if (settings[key]) {
          settings[key] = storage[key].value;
        }

        return settings;
      }, {})
    )
  ),
  setSetting: jest.fn((key: string, value?: string) => {
    if (storage[key]) {
      storage[key].value = value;
    }
    return Promise.resolve();
  }),
  getSetting: jest.fn((key: string) => {
    if (storage[key]) {
      return Promise.resolve(storage[key].value || false);
    }

    return Promise.resolve(false);
  }),
  onSettingsChange: jest.fn(),
};

export const registerPluginSetting: RegisterServerOptions['registerSetting'] = jest.fn(
  (options: RegisterServerSettingOptions) => {
    // @ts-ignore
    storage[options.name] = options;
    return Promise.resolve();
  }
);
