// {
//     "lang": "ar",
//     "lang_text": "Arabic",
//     "md5": "d912e25ce46f94fe362b8c98e5efb4fd",
//     "name": "vosk-model-ar-mgb2-0.4",
//     "obsolete": "false",
//     "size": 333241610,
//     "size_text": "317.8MiB",
//     "type": "big",
//     "url": "https://alphacephei.com/vosk/models/vosk-model-ar-mgb2-0.4.zip",
//     "version": "mbg2-0.4"
// },
// https://alphacephei.com/vosk/models/model-list.json

export type VoskModelType = 'big' | 'big-lgraph' | 'small';

export interface VoskModelDescriptor {
  lang: string; // shall be taken as a locale and not a language
  lang_text: string;
  md5: string;
  name: string;
  obsolete: boolean;
  size: number;
  size_text: string;
  type: VoskModelType;
  url: string;
  version: string;
}
