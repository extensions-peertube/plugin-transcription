# Development

1. Install and start a **PeerTube** _dev_ instance following this guide:
   https://docs.joinpeertube.org/contribute-plugins?id=test-your-plugintheme

> _In dev mode, administrator username is **root** and password is **test**._

2. Install the plugin from your **PeerTube** directory:

```shell
yarn plugin:install --plugin-path ~/workspace/peertube/peertube-plugin-transcription
```

3. From the plugin directory:

```shell
yarn link
cd storage/plugins
yarn link "peertube-plugin-transcription"
```

## Debuggging

On a __PeerTube__ production environment logs are usually found at `/var/www/peertube/storage/logs/peertube.log`.
To follow the relevant parts of the logs you may use `grep`:
```shell
tail -f /var/www/peertube/storage/logs/peertube.log | grep peertube-plugin-transcription
```
> These lines will be relevant to installation, configuration and plugin settings issues.

There are also logs for transcription workers, for example, you'll find worker n°1 log at:
```shell
cat /var/www/peertube/storage/plugins/data/peertube-plugin-transcription/worker-1.log
```

## Continuous Integration

> See `.gitlab-ci.yml`.

## Continuous deployment

Set up a remote host for automatic deployment.

As your `peertube` user, create a key pair, and add it the list of the authorized key:

```shell
mkdir ~/.ssh
ssh-keygen -t rsa -b 2048 -C "PeerTube plugins deployment key" -f ~/.ssh/id_rsa -N ""
cat ~/.ssh/id_rsa.pub > ~/.ssh/authorized_keys
```

Add the content of the private key file as a new variable `DEPLOYER_SSH_PRIVATE_KEY` under project "Settings > CI/CD > Variables.
Don't forget to protect this variable by setting **protect** and **mask** flags.

> See https://docs.gitlab.com/ee/ci/ssh_keys/#ssh-keys-when-using-the-docker-executor for more details.

## Resources

- https://docs.joinpeertube.org/contribute-plugins
- https://framagit.org/framasoft/peertube/official-plugins/-/blob/master/peertube-plugin-hello-world/main.js
